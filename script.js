let trainer = {

	name:"Ash Ketchum",
	age: 10,
	pokemon: [
		"Pikachu",
		"Charizard",
		"Squirtle",
		"Bulbasaur"
	],
	friends: { 
		hoen: ["May","Max"],
		kanto:["Brock","Misty"]
	},
	talk: function(){
		return `${this.pokemon[0]}! I choose you ` 
	}
}
console.log(trainer);

console.log(trainer.name);
console.log(trainer["pokemon"]);
console.log(trainer.talk());


function Pokemon(name, lvl){
	this.name = name;
	this.level = lvl;
	this.health = lvl * 2;
	this.attack = lvl * 2;
	this.tackle = this.attack;
	this.intro = function(opponent){
		let tackle = opponent.health -= this.tackle
		console.log(`${this.name} tackled ${opponent.name}`)
		console.log(`${opponent.name}'s health is now reduced to ${tackle}`)
		
		if (opponent.health <= 0){ 
			console.log(`${opponent.name} has fainted`)
		}
		console.log(opponent);
		

	}
}




let pickachu = new Pokemon("Pickachu",12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100)

console.log(pickachu);
console.log(geodude);
console.log(mewtwo);


geodude.intro(pickachu);
mewtwo.intro(geodude);












